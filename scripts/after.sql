-- ####RUN THIS SCRIPT AFTER UPDATE THE MODULE JUST ONCE!

------ ### changing the field type and migration from boolean to string equivalence
-- UPDATE gnuhealth_contact_tracing_call SET anosmia='yes' WHERE anosmia_drop=true;
-- UPDATE gnuhealth_contact_tracing_call SET anosmia='no' WHERE anosmia_drop=false OR anosmia IS Null;
-- 
-- UPDATE gnuhealth_contact_tracing_call SET arthralgia='yes' WHERE arthralgia_drop=true;
-- UPDATE gnuhealth_contact_tracing_call SET arthralgia='no' WHERE arthralgia_drop=true OR arthralgia IS Null;
-- 
-- UPDATE gnuhealth_contact_tracing_call SET diarrhea_vomit='yes' WHERE diarrhea_drop=true AND vomits=true;
-- UPDATE gnuhealth_contact_tracing_call SET diarrhea_vomit='no' WHERE diarrhea_vomit IS Null;
-- 
-- UPDATE gnuhealth_contact_tracing_call SET dysgeusia='yes' WHERE dysgeusia_drop=true;
-- UPDATE gnuhealth_contact_tracing_call SET dysgeusia='no' WHERE dysgeusia_drop=false OR dysgeusia IS Null;
-- 
-- UPDATE gnuhealth_contact_tracing_call SET dyspnoea='yes' WHERE dyspnoea_drop=true;
-- UPDATE gnuhealth_contact_tracing_call SET dyspnoea='no' WHERE dyspnoea_drop=false OR dyspnoea IS Null;
-- 
-- UPDATE gnuhealth_contact_tracing_call SET fever_high='yes' WHERE fever_high_drop=true OR fever_low=true;
-- UPDATE gnuhealth_contact_tracing_call SET fever_high='no' WHERE fever_high_drop=false AND fever_low =false OR fever_high IS Null;
-- 
-- UPDATE gnuhealth_contact_tracing_call SET myalgia='yes' WHERE myalgia_drop=true;
-- UPDATE gnuhealth_contact_tracing_call SET myalgia='no' WHERE myalgia_drop=false OR myalgia IS Null;
-- 
-- UPDATE gnuhealth_contact_tracing_call SET cough='yes' WHERE cough_drop=true;
-- UPDATE gnuhealth_contact_tracing_call SET cough='no' WHERE cough_drop=false OR cough IS Null;
-- 
-- UPDATE gnuhealth_contact_tracing_call SET headache='yes' WHERE headache_drop=true;
-- UPDATE gnuhealth_contact_tracing_call SET headache='no'WHERE headache_drop=false OR headache IS Null;
-- 
-- UPDATE gnuhealth_contact_tracing_call SET introspective_health='good';
-- UPDATE gnuhealth_contact_tracing_call SET evolution='n' WHERE evolution IS Null;
-- 
-- ALTER TABLE gnuhealth_contact_tracing_call DROP COLUMN anosmia_drop;
-- ALTER TABLE gnuhealth_contact_tracing_call DROP COLUMN arthralgia_drop;
-- ALTER TABLE gnuhealth_contact_tracing_call DROP COLUMN diarrhea_drop;
-- ALTER TABLE gnuhealth_contact_tracing_call DROP COLUMN dysgeusia_drop;
-- ALTER TABLE gnuhealth_contact_tracing_call DROP COLUMN dyspnoea_drop;
-- ALTER TABLE gnuhealth_contact_tracing_call DROP COLUMN fever_high_drop;
-- ALTER TABLE gnuhealth_contact_tracing_call DROP COLUMN myalgia_drop;
-- ALTER TABLE gnuhealth_contact_tracing_call DROP COLUMN cough_drop;
-- ALTER TABLE gnuhealth_contact_tracing_call DROP COLUMN headache_drop;

-------#####setting to the default covid-19 campaign all the eno and the contact tracing records at the moment
-- UPDATE gnuhealth_contact_tracing SET campaign ='1' WHERE campaign IS Null;
-- UPDATE gnuhealth_contact_tracing_eno SET campaign ='1' WHERE campaign IS Null;


------####setting up conglomerate_institution from health institution to a party institution
--  UPDATE gnuhealth_contact_tracing_eno SET conglomerate_institution = new_values.conglomerate_institution FROM new_values WHERE new_values.id = gnuhealth_contact_tracing_eno.id;
--  DROP TABLE new_values;

 
 -----#####change context and contact_type fields char into many2one selection widget
-- INSERT INTO gnuhealth_contact_tracing_context  (name)  SELECT contact_type_drop FROM "gnuhealth_contact_tracing_eno-party" WHERE contact_type_drop IS NOT NULL;
-- INSERT INTO gnuhealth_contact_tracing_context  (name)  SELECT context FROM gnuhealth_contact_tracing WHERE context IS NOT NULL;
-- 
-- UPDATE "gnuhealth_contact_tracing_eno-party" SET contact_type = gnuhealth_contact_tracing_context.id FROM gnuhealth_contact_tracing_context WHERE "gnuhealth_contact_tracing_eno-party".contact_type_drop= gnuhealth_contact_tracing_context.name;
-- UPDATE gnuhealth_contact_tracing SET context_selection = gnuhealth_contact_tracing_context.id FROM gnuhealth_contact_tracing_context WHERE gnuhealth_contact_tracing.context = gnuhealth_contact_tracing_context.name;
-- 
-- ALTER TABLE "gnuhealth_contact_tracing_eno-party" DROP COLUMN contact_type_drop;


