from trytond.model import ModelView, ModelSQL, fields, Unique
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Or, Not, Bool, Equal


class ENO(ModelSQL,ModelView):
    'Mandatory Notification - Enfermedad Notificación Obligatoria'
    __name__ = 'gnuhealth.contact_tracing.eno'
    '''
    Campaign
    '''
    campaign = fields.Many2One('gnuhealth.contact_tracing.campaign','Campaign',
        required=True,states={'readonly': Eval('state')=='done'})
    '''
    CLASSIFICATION AT THE NOTIFICATION MOMENT
    '''
    case_status = fields.Selection([
        ('suspicius','Suspicius case on investigation'),
        ('clinical_confirmed','Case confirmed by clinical-epidemiological criteria'),
        ('antigenous_confirmed','Case confirmed by field antigen test'),
        ], 'Case status',sort=False, required=True,
        states={ 'readonly': Eval('state')=='done'})
    '''
    ------------------Institution identification-------------------------------
    '''
    institution = fields.Many2One('party.party','Institution',
        help="Institution that notifies",
        domain= [('is_institution','=',True)],
        required=True,states={'readonly': Eval('state')=='done'})
    city = fields.Char('City',
        required=True,states={'readonly': Eval('state')=='done'})
    province = fields.Many2One('country.subdivision','Province',
        required=True,states={'readonly': Eval('state')=='done'})
    notification_date = fields.Date('Notification Date',
        required=True,states={'readonly': Eval('state')=='done'})
    health_prof = fields.Many2One('gnuhealth.healthprofessional',
        'Health Professional', required=True,
        states={'readonly': Eval('state')=='done'})
    cellphone = fields.Char('Cellphone',
        help="Institution cellphone",required=True,
        states={'readonly': Eval('state')=='done'})
    email = fields.Char('Email', help="Institution email",
        states={'readonly': Eval('state')=='done'})
    health_prof_institution_role = fields.Char(
        'Role inside the system',help="Medic, nurse, administrator, etc",
        states={'readonly': Eval('state')=='done'})

    '''
    ------------------Case identification-------------------------------
    Case/event ID on the National National 
    Health Surveillance System (SNVS Argentina)
    '''
    id_event_snvs_case = fields.Char('Event ID / SNVS Case',
        states={'readonly': Eval('state')=='done'})
    patient = fields.Many2One('gnuhealth.patient','Patient',
        required=True,states={'readonly': Eval('state')=='done'})
    ref = fields.Function(fields.Char('PUID'),'get_ref')
    citizenship = fields.Many2One('country.country','Nationality',
        required=True,states={'readonly': Eval('state')=='done'})
    deprived_liberty = fields.Selection([
        (None,''),
        ('yes','Yes'),
        ('no','No'),
        ],'Deprived liberty',sort=False,required=True,
        states={'readonly': Eval('state')=='done'})
    lives_in_elderly = fields.Selection([
        ('no','No'),
        ('yes','Yes'),
        ],'Live in a residence for the elderly',sort=False, required=True,
        states={'readonly': Eval('state')=='done'})
    indigenous_people = fields.Selection([
        (None,''),
        ('yes','Yes'),
        ('no','No'),
        ],'Indigenous people',sort=False,required=True,
        states={'readonly': Eval('state')=='done'})
    ethnicity = fields.Char('Ethnicity',
        states={
            'readonly': Or(Not(Equal(Eval('indigenous_people'),'yes')),Eval('state')=='done'),
            'required': Equal(Eval('indigenous_people'),'yes'),
            })
    patient_province = fields.Many2One(
        'country.subdivision','Province',help="Patient Province",
        required=True,states={'readonly': Eval('state')=='done'})
    department = fields.Char('Department',required=True,
        states={'readonly': Eval('state')=='done'})
    patient_city = fields.Char('City',required=True,
        states={'readonly': Eval('state')=='done'})
    street = fields.Char('Street',required=True,
        states={'readonly': Eval('state')=='done'})
    house_number = fields.Char('House Number',required=True,
        states={'readonly': Eval('state')=='done'})
    floor = fields.Char('Floor',required=True,
        states={'readonly': Eval('state')=='done'})
    build_department = fields.Char('Build department',required=True,
        states={'readonly': Eval('state')=='done'})
    zip_code = fields.Char('Zip code',required=True,
        states={'readonly': Eval('state')=='done'})
    neighboard = fields.Char('Neighboard',required=True,
        states={'readonly': Eval('state')=='done'})
    patient_phone = fields.Char('Phone',help="Patient phone",required=True,
        states={'readonly': Eval('state')=='done'})
    dob = fields.Function(fields.Date('Date of birth'),'get_dob')
    age = fields.Function(fields.Char('Age'),'get_age')
    gender = fields.Selection([
        (None,''),
        ('m','Male'),
        ('f','Female'),
        ],'Gender',sort=False,required=True,
        states={'readonly': Eval('state')=='done'})
    '''
    ----------------Clinical Information---------------------------------
    '''
    first_symptoms_date = fields.Date('First symptoms date',required=True,
        states={'readonly': Eval('state')=='done'})
    epidemiological_week_fsd = fields.Function(
        fields.Integer('Epidemiological Week of\n first symptoms date'),
        'get_ep_week_fsd')
    patient_condition = fields.Selection([
        (None,''),
        ('inpatient','Inpatient'),
        ('ambulatory','Ambulatory'),
        ],'Patient Condition',required=True,
        states={'readonly': Eval('state')=='done'})
    first_consult_date = fields.Date('First consult date',required=True,
        states={'readonly': Eval('state')=='done'})
    first_consult_institution = fields.Many2One('gnuhealth.institution',
        'First consult institution',
        required=True,states={'readonly': Eval('state')=='done'})
    deceased_case = fields.Boolean('Deceased case',
        states={'readonly': Eval('state')=='done'})
    place_of_death = fields.Selection([
        (None,''),
        ('health_institution','Deceased on Health Institution'),
        ('at_home','Deceased at home'),
        ('other_place','Other'),
        ], 'Place of death',sort=False,
        states={
            'invisible': Not(Bool(Eval('deceased_case'))),
            'required': Bool(Eval('deceased_case')),
            'readonly': Eval('state')=='done',
            })
    other_place_of_death = fields.Char('Other place of death',
        states={
            'invisible': Eval('place_of_death')!='other_place',
            'required': Eval('place_of_death')=='other_place',
            'readonly': Eval('state')=='done',
            })
    '''
    --------------Signs and symptoms--------------------
    '''
    has_acute_anosmia = fields.Boolean('Accute anosmia',
        help="Recently appearing of anosmia (loss of smell)",
        states={'readonly': Eval('state')=='done'})
    has_acute_disgeusia = fields.Boolean('Acute Disgeusia',
        help='Recently appearing of disgeusia (loss of taste)',
        states={'readonly': Eval('state')=='done'})
    fever = fields.Selection([
        (None,''),
        ('no','No fever'),
        ('more_than_38','More than 38'),
        ('less_than_38','Less than 38'),
        ],'Fever',sort=False,required=True,
        states={'readonly': Eval('state')=='done'})
    has_cough = fields.Boolean('Cough',
        states={'readonly': Eval('state')=='done'})
    signs_symptoms_observations = fields.Char('Observations',
        states={'readonly': Eval('state')=='done'})   
    has_respiratory_difficult = fields.Boolean(
        'Respiratory difficult',
        states={'readonly': Eval('state')=='done'})   
    has_odinofagia = fields.Boolean('Odinofagia',
        help="Pain when swallowing",
        states={'readonly': Eval('state')=='done'})
    has_cephalea = fields.Boolean('Cephalea',help="Headache",
        states={'readonly': Eval('state')=='done'})
    '''has_coma = fields.Boolean('Coma',
        states={'readonly': Eval('state')=='done'})'''
    has_mialgias = fields.Boolean('Mialgias',help="Muscular pain",
        states={'readonly': Eval('state')=='done'})
    has_diarrea_vomits = fields.Boolean('Diarrea/Vomits',
        states={'readonly': Eval('state')=='done'})
    has_another = fields.Boolean('Another',
        states={'readonly': Eval('state')=='done'})
    specific = fields.Char('Specific',
        states={'readonly': Eval('state')=='done'})
        
    '''
    has_mental_confusion = fields.Boolean('Mental confusion',
        states={'readonly': Eval('state')=='done'})
    has_convulsions = fields.Boolean('Convulsions',
        states={'readonly': Eval('state')=='done'})
    has_disnea = fields.Boolean('Disenea/Takipnea (RF > 25)',
        states={'readonly': Eval('state')=='done'})
    has_abdominal_pain = fields.Boolean('Abdominal pain',
        states={'readonly': Eval('state')=='done'})
    has_torax_pain = fields.Boolean('Torax pain',
        states={'readonly': Eval('state')=='done'})
    has_neumonia_rad_evidence = fields.Boolean(
        'Clinical and radiological evicende of neumonia',
        states={'readonly': Eval('state')=='done'})
        
    has_conjunctival_injection = fields.Boolean('Conjunctival injection',
        states={'readonly': Eval('state')=='done'})
    has_irritability = fields.Boolean('Irritability',
        states={'readonly': Eval('state')=='done'})
    has_general_discomfort = fields.Boolean('General discomfort',
        states={'readonly': Eval('state')=='done'})
    
    has_acute_neumonia = fields.Boolean(
        'Acute neumonia (requires hospitalization)',
        states={'readonly': Eval('state')=='done'})
        has_food_refusal = fields.Boolean('Food refuzal',
        states={'readonly': Eval('state')=='done'})
    has_tiraje = fields.Boolean('Tiraje',
        states={'readonly': Eval('state')=='done'})
    
    
    has_artralgia = fields.Boolean('Artralgia',help="Joint discomfort",
        states={'readonly': Eval('state')=='done'})'''
    '''
    ------------------MULSYSTEMIC INFLAMATORY SYNDROME DX----------------------
    '''
    cutaneous_eruption_and_others = fields.Boolean(
        'Cutaneous eruption / non-purulent bilateral conjunctivitis / '
        'sign of mucocutaneous inflammation (oral, hands or feet)',
        states={'readonly': Eval('state')=='done'})
    hipotension_shock = fields.Boolean(
        'Hypotension / shock',
        states={'readonly': Eval('state')=='done'})
    pericarditis_and_others = fields.Boolean(
        'Pericarditis / valvulitis / coronary abnormalities',
        states={'readonly': Eval('state')=='done'})
    coagulopathy_and_other = fields.Boolean(
        'Coagulopathy Diarrhea, vomits or abdominal_pain',
        states={'readonly': Eval('state')=='done'})
    elevated_inflamatory_marquers = fields.Boolean(
        'Elevated markers of inflammation',
        states={'readonly': Eval('state')=='done'})

    '''
    --------------Comorbilities and previous pathologies--------------------------
    '''
    comorbilities = fields.Selection([
        (None,''),
        ('yes','Yes'),
        ('no','No'),
        ],'Comorbilities',sort=False,required=True,
        states={'readonly': Eval('state')=='done'})
    has_asthma = fields.Boolean('Asthma',
        states={'readonly': Eval('state')=='done'})
    had_low_weight_birth = fields.Boolean('Low weight birth',
        states={'readonly': Eval('state')=='done'})
    has_previous_bronquiolitis = fields.Boolean('Previous bronquilitis',
        states={'readonly': Eval('state')=='done'})
    has_diabetes = fields.Boolean('Diabetes',
        states={'readonly': Eval('state')=='done'})
    dialisis = fields.Selection([
        (None,''),
        ('no','No'),
        ('acute','Acute'),
        ('chronic','Chronic'),
        ],'Dialisis',sort=False,required=True,
        states={'readonly': Eval('state')=='done'})
    has_pregnancy = fields.Boolean('Pregnancy',
        states={'readonly': Eval('state')=='done'})
    has_liver_disease = fields.Boolean('Liver disease',
        states={'readonly': Eval('state')=='done'})
    has_neurological_disease = fields.Boolean('Neurological disease',
        states={'readonly': Eval('state')=='done'})
    has_oncological_disease = fields.Boolean('Oncological disease',
        states={'readonly': Eval('state')=='done'})
    has_chronic_kidney_disease = fields.Boolean('Chronic kidney disease - No dialisis',
        states={'readonly': Eval('state')=='done'})
    has_COLD = fields.Boolean('COLD',help='Chronic Obstructive Lung Disease',
        states={'readonly': Eval('state')=='done'})
    smoker = fields.Selection([
        (None,''),
        ('no','No'),
        ('former','former smoker'),
        ('currently','Currently smoker')
        ],'Smoker',sort=False,required=True,
        states={'readonly': Eval('state')=='done'})
    has_cardiopathy = fields.Boolean(
        'Cardiopathy',
        states={'readonly': Eval('state')=='done'})
    has_arterial_hypertension = fields.Boolean('Arterial hypertension',
        states={'readonly': Eval('state')=='done'})
    has_immunosuppression = fields.Boolean(
        'Congenit or aquired \nimmunosuppression',
        states={'readonly': Eval('state')=='done'})
    has_ACN_previous = fields.Boolean('Previous ACN',
        help='Previous Aquired Community Neumonia',
        states={'readonly': Eval('state')=='done'})
    none_of_previous = fields.Boolean('None of the previous',
        states={'readonly': Eval('state')=='done'})
    is_premature = fields.Boolean('Premature',
        states={'readonly': Eval('state')=='done'})
    is_puerperium = fields.Boolean('Puerperium',
        states={'readonly': Eval('state')=='done'})
    has_cerebrovascular_disease = fields.Boolean(
        'Cerebrovascular disease',
        states={'readonly': Eval('state')=='done'})
    has_peritoneal_dialisis = fields.Boolean(
        'Peritoneal dialisis',
        states={'readonly': Eval('state')=='done'})
    has_obesity = fields.Boolean('Obesity (BMI  equal or greater than 35)',
        states={'readonly': Eval('state')=='done'})
    has_kidney_transplant = fields.Boolean(
        'Kidney transplanted',
        states={'readonly': Eval('state')=='done'})
    has_another_comorbilities = fields.Boolean('Another',
        states={'readonly': Eval('state')=='done'})
    comorbilities_specific = fields.Char('Specific',
        states={'readonly': Eval('state')=='done'})
    comorbilities_observations = fields.Char('Observations',
        states={'readonly': Eval('state')=='done'})


    '''
    has_heart_failure = fields.Boolean('Heart failure',
        states={'readonly': Eval('state')=='done'})
   
   
            
    has_tuberculosis = fields.Boolean('Tuberculosis',
        states={'readonly': Eval('state')=='done'})
    
    '''
    '''
    -----------Diagnostic-----------------------
    '''
    has_severe_acute_respiratory_infections = fields.Boolean(
        'Severe Acute Respiratory Infections\n (Fever + Cough + Need for hospitalization)',
        states={'readonly': Eval('state')=='done'})
    has_influenza_like_illness = fields.Boolean('Influenza like illness',
        states={'readonly': Eval('state')=='done'})
    has_neumonia = fields.Boolean(
        'Neumonia\n with suggestives COVID-19 torax images',
        states={'readonly': Eval('state')=='done'})
    other_diagnostics = fields.Char('Other',
        states={'readonly': Eval('state')=='done'})
    '''
    ----------------Treatment-------------
    '''
    drug_treatment = fields.One2Many(
        'gnuhealth.contact_tracing.eno.drug_treatment','eno','Drug Treatment',
        states={'readonly': Eval('state')=='done'})
    convalecient_plasma_start_date = fields.Date('Convalecient plasma start date',
        states={'readonly': Eval('state')=='done'})
    convalecient_plasma_end_date = fields.Date('Convalecient plasma end date',
        states={'readonly': Eval('state')=='done'})
    mra_start_date = fields.Date('Mechanical respiratory assitence start date',
        states={'readonly': Eval('state')=='done'})
    mra_end_date = fields.Date('Mechanical respiratory assitence end date',
        states={'readonly': Eval('state')=='done'})
    another_treatment = fields.Char('Another treatment',
        states={'readonly': Eval('state')=='done'})
    another_treatment_start_date = fields.Date('Another treatment start date',
        states={'readonly': Eval('state')=='done'})
    another_treatment_end_date = fields.Date('Another treatment end date',
        states={'readonly': Eval('state')=='done'})
    '''
    -------------------Evolution-------------------------
    '''
    hospitalization_institution = fields.Many2One('gnuhealth.institution',
        'Institution',states={'readonly': Eval('state')=='done'})
    hospitalization_by_clinical = fields.Boolean(
        'Hospitalization by clinical criterea',
        states={'readonly': Eval('state')=='done'})
    hospitalization_date = fields.Date('Hospitalization date',
        states={'readonly': Eval('state')=='done'})
    hospitalization_uci = fields.Boolean('Hospitalization on UCI',
        states={'readonly': Eval('state')=='done'})
    hospitalization_uci_date = fields.Date('Hospitalization on UCI date',
        states={'readonly': Eval('state')=='done'})
    discharge = fields.Selection([
        (None,''),
        ('no','No'),
        ('discharge','Discharge'),
        ('passed','Passed away'),
        ],'Hospitalization status',sort=False,required=True,
        states={'readonly': Eval('state')=='done'})
    discharge_date = fields.Date('Discharge date',
        states={'readonly': Eval('state')=='done'})
    has_mra_required = fields.Boolean('MRA required',
        help="Mechanical Respiratory Assistence Required",
        states={'readonly': Eval('state')=='done'})
    has_mra_required_date = fields.Boolean('Date',
        states={'readonly': Eval('state')=='done'})
    '''Good Evolution'''
    has_issolated_no_hospital_center = fields.Boolean(
        'Issolation on non-hospital center',
        states={'readonly': Eval('state')=='done'})
    has_issolated_hospital = fields.Boolean('Issolation on hospital',
        states={'readonly': Eval('state')=='done'})
    has_issolated_domiciliary = fields.Boolean('Issolation domiciliary',
        states={'readonly': Eval('state')=='done'})
    '''
    --------------Epidemiological History-------------
    '''
    '''Ocupation'''
    is_health_prof_assistential = fields.Boolean(
        'Health Professional with asistential functions',
        states={'readonly': Eval('state')=='done'})
    is_health_prof_technician_aux = fields.Boolean(
        'Health Professional\n technician/aux\
        with asistential functions',
        states={'readonly': Eval('state')=='done'})
    is_health_prof_administrative = fields.Boolean(
        'Health Professional\n with administrative functions',
        states={'readonly': Eval('state')=='done'})
    resides_or_work_on_demi_closed_institution = fields.Boolean(
        'Resides or work on a demi-close institution or extended stay?',
        states={'readonly': Eval('state')=='done'})
    institution_residence_or_work = fields.Many2One('party.party','Which one?',
        states={
            'readonly': Or(Eval('state')=='done',Not(Bool(Eval('resides_or_work_on_demi_closed_institution'))))
            })
    '''--------------Anti-flu vaccine history---------------------'''
    has_anti_flu_vaccine = fields.Boolean('Anti-flu vaccine history',
        states={'readonly': Eval('state')=='done'})
    anti_flu_vaccine_date = fields.Date('Anti-flu vaccine date',
        states={'readonly': Eval('state')=='done'})
    
    
    '''--------------Travels and risk exposures---------------------'''
    has_been_close_contact_cohabiting = fields.Boolean(
        '1. Has been in close contact (cohabiting) with a positive case of COVID-19',
        states={'readonly': Eval('state')=='done'})
    lastname_name_case_cohabiting = fields.Char('Lastname - name of the case',
        states={'readonly': Eval('state')=='done'})
    id_contact_case_cohabiting = fields.Char('PUID or ID NHSS (SNVS - Argentina)',
        states={'readonly': Eval('state')=='done'})
    has_been_close_contact_not_cohabiting = fields.Boolean(
        '2. Has been in close contact (NOT cohabiting) with a positive case of COVID-19',
        states={'readonly': Eval('state')=='done'})
    lastname_name_case_not_cohabiting= fields.Char('Lastname - name of the case',
        states={'readonly': Eval('state')=='done'})
    id_contact_case_not_cohabiting = fields.Char('PUID or ID NHSS (SNVS - Argentina)',
        states={'readonly': Eval('state')=='done'})
    has_been_contact_with_laboratory_cases= fields.Boolean(
        '3. has been in contact with positive cases of covid-19 by laboratory agglomerate.',
        states={'readonly': Eval('state')=='done'})
    has_been_contact_health_center= fields.Boolean(
        '4. has been in contact with a positive case of covid-19 in a health center.',
        states={'readonly': Eval('state')=='done'})
    health_center= fields.Many2One('gnuhealth.institution',
        'Institution',states={'readonly': Eval('state')=='done'})
    is_health_prof_patient_contact = fields.Boolean(
        '5. Is a health professional and may have acquired '
        'the infection while treating Covid-19 patients?',
        states={'readonly': Eval('state')=='done'})
    is_health_prof_coworker_contact = fields.Boolean(
        '6. Is health professional and may you have '
        'acquired the infection through contact with another professional?',
        states={'readonly': Eval('state')=='done'})
    has_traveled_outside_covid19 = fields.Boolean(
        '7. Has traveled to a risk zone '
        'for COVID-19 OUTSIDE THE COUNTRY?',
        states={'readonly': Eval('state')=='done'})
    name_place_outside= fields.Char('Name place',
        states={'readonly': Eval('state')=='done'})
    traveled_date_outside = fields.Date('Traveled date',
        states={'readonly': Eval('state')=='done'})
    has_traveled_inside_covid19 = fields.Boolean(
        '8 .Has traveled to a risk zone '
        'for COVID-19 INSIDE THE COUNTRY?',
        states={'readonly': Eval('state')=='done'})
    name_place_inside= fields.Char('Name place',
        states={'readonly': Eval('state')=='done'})
    traveled_date_inside = fields.Date('Traveled date',
        states={'readonly': Eval('state')=='done'})
    is_community_transm = fields.Boolean(
        '9. Is a possible community transmission?',
        states={'readonly': Eval('state')=='done'})
    has_been_contact_asymptomatic_case = fields.Boolean(
        '10. has been in contact with an asymptomatic case?',
        states={'readonly': Eval('state')=='done'})

    
    '''has_been_close_contact = fields.Boolean(
        '3. Has been in close contact with a positive COVID-19 case'
        'on the last 14 days',
        states={'readonly': Eval('state')=='done'})
    lastname_name_case = fields.Char('Lastname - name of the case',
        states={'readonly': Eval('state')=='done'})
    id_contact_case = fields.Char('PUID or ID NHSS (SNVS - Argentina)',
        states={'readonly': Eval('state')=='done'})
    has_health_center_attention = fields.Boolean(
        '4. Has received attention on a health center'
        'that attends COVID-19 cases on the last 14 days?',
        states={'readonly': Eval('state')=='done'}) 
    is_health_prof_coworker_contact = fields.Boolean(
        '5. Is a health professional and could have been infected'
        'from a coworker?',
        states={'readonly': Eval('state')=='done'})
    is_health_prof_unknown_contact = fields.Boolean(
        '6. Is a health professional and unknow the'
        'epidemiological nexus',
        states={'readonly': Eval('state')=='done'})
    has_health_prof_close_positive = fields.Boolean(
        '7. Has assisted, as a health professional, positive COVID-19'
        'cases on the last 14 days?',
        states={'readonly': Eval('state')=='done'})
    is_community_transm = fields.Boolean(
        '8. Is a possible community transmission?',
        states={'readonly': Eval('state')=='done'})
    belongs_to_institutional = fields.Boolean(
        '9. Belongs to an institutional conglomerate of cases',
        states={'readonly': Eval('state')=='done'})
    is_hospital_clinicals = fields.Boolean('Hospital/Assitential Clinic',
        states={'readonly': Eval('state')=='done'})
    is_mental_institution = fields.Boolean('Mental Health Institution',
        states={'readonly': Eval('state')=='done'})
    is_penitentiary = fields.Boolean('Penintetiary institution',
        states={'readonly': Eval('state')=='done'})
    is_residence_elderly = fields.Boolean('Residence for the elderly',
        states={'readonly': Eval('state')=='done'})
    is_another_institution = fields.Boolean('Another',
        states={'readonly': Eval('state')=='done'})
    conglomerate_institution = fields.Many2One('party.party','Institution',
        domain=[('is_institution','=',True)],
        states={'readonly': Eval('state')=='done'})'''
    '''
    -------------Laboratory-----------------------
    '''
    '''Samples'''
    is_aspirated = fields.Boolean('Aspirated',
        states={'readonly': Eval('state')=='done'})
    is_swabbing = fields.Boolean('Swabbing',
        states={'readonly': Eval('state')=='done'})
    is_sputum = fields.Boolean('Sputum',
        states={'readonly': Eval('state')=='done'})
    is_bronchoalveolar_lavage = fields.Boolean('Bronchoalveolar lavage',
        states={'readonly': Eval('state')=='done'})
    is_spittle= fields.Boolean('spittle',
        states={'readonly': Eval('state')=='done'})
    '''another_sample = fields.Boolean('Another sample',
        states={
            'readonly':Eval('state')=='done',
            'invisible':Not(Bool(Eval('is_another_sample')))
               })    '''
    another_sample_char= fields.Char('another',
        states={'readonly': Eval('state')=='done'})
    '''others options '''
    sample_institution = fields.Many2One('gnuhealth.institution',
        'Institution that gets the sample',
        states={'readonly': Eval('state')=='done'})
    sample_date = fields.Date('Sample date',
        states={'readonly': Eval('state')=='done'})
    derived_sample_institution = fields.Many2One('gnuhealth.institution',
        'Institution where the samples are derived',
        states={'readonly': Eval('state')=='done'})
    derivation_date = fields.Date('Derivation date',
        states={'readonly': Eval('state')=='done'})
    '''------------------Party close contacts----------------------'''
    party_contacts = fields.One2Many('gnuhealth.contact_tracing.eno-party',
        'eno','People that has been in close contact during the sickness',
        states={'readonly': Eval('state')=='done'})
    party_contacts_observations = fields.Char('Observations',
        states={'readonly': Eval('state')=='done'})
    '''Personal that notifies data'''
    health_prof_notif = fields.Many2One('gnuhealth.healthprofessional',
        'Health Professional Notification',
        states={'readonly': Eval('state')=='done'})
    state = fields.Selection([
        (None,''),
        ('draft','Draft'),
        ('done','Done'),
        ],'State', readonly=True)
    contact_tracing = fields.One2Many(
        'gnuhealth.contact_tracing','eno','Contact Tracing',readonly=True)

    def get_dob(self, name):
        if self.patient.dob:
            return self.patient.dob

    def get_age(self, name):
        age = ''
        if self.patient.name.dob:
            age = self.notification_date.year -self.patient.name.dob.year
        return str(age)

    def get_ref(self, name):
        return self.patient.puid

    def get_ep_week_fsd(self, name):
        return self.on_change_with_epidemiological_week_fsd()

    @fields.depends('first_symptoms_date')
    def on_change_with_epidemiological_week_fsd(self):
        if self.first_symptoms_date:
            return self.first_symptoms_date.isocalendar()[1]
        return None

    @fields.depends('institution')
    def on_change_institution(self):
        if self.institution:
            self.city = self.institution.addresses and self.institution.addresses[0].city
            self.province = self.institution.addresses\
                and self.institution.addresses[0].subdivision\
                and self.institution.addresses[0].subdivision.id
            self.cellphone = self.institution.contact_mechanisms\
                and '/ '.join([x.value for x in self.institution.contact_mechanisms
                               if x.type in ['phone','mobile']])
            self.email = self.institution.contact_mechanisms\
                and '/ '.join([x.value for x in self.institution.contact_mechanisms
                               if x.type in ['email']])
        else:
            self.city = None
            self.province = None
            self.cellphone = None
            self.email = None

    @fields.depends('patient')
    def on_change_patient(self):
        if self.patient:
            self.ref = self.patient.puid
            self.citizenship =\
                self.patient.name.citizenship and self.patient.name.citizenship.id
            self.patient_province =\
                self.patient.name.du and self.patient.name.du.address_subdivision\
                    and self.patient.name.du.address_subdivision.id
            self.patient_city =\
                self.patient.name.du and self.patient.name.du.address_city
            self.street =\
                self.patient.name.du and self.patient.name.du.address_street
            self.house_number =\
                self.patient.name.du and str(self.patient.name.du.address_street_number or '')
            self.floor =\
                self.patient.name.du and self.patient.name.du.address_street_bis
            self.zip_code =\
                self.patient.name.du and self.patient.name.du.address_zip
            self.neighboard =\
                self.patient.name.du and self.patient.name.du.address_district
            self.patient_phone = self.patient.name.contact_mechanisms\
                and '/ '.join([x.value for x in self.patient.name.contact_mechanisms
                               if x.type in ['phone','mobile']])
            self.dob = self.patient.dob
            self.gender = self.patient.gender
        else:
            self.ref = None
            self.citizenship = None
            self.patient_province = None
            self.patient_city = None
            self.street = None
            self.house_number = None
            self.floor = None
            self.zip_code = None
            self.neighboard = None
            self.patient_phone = None
            self.dob = None
            self.age = None

    @staticmethod
    def default_campaign():
        pool = Pool()
        Configuration = pool.get('gnuhealth.contact_tracing.configuration')(1)
        if Configuration.current_campaign:
            return int(Configuration.current_campaign)

    @staticmethod
    def default_deprived_liberty():
        return 'no'

    @staticmethod
    def default_indigenous_people():
        return 'no'

    @staticmethod
    def default_patient_condition():
        return 'ambulatory'

    @staticmethod
    def default_dialisis():
        return 'no'

    @staticmethod
    def default_smoker():
        return 'no'

    @staticmethod
    def default_fever():
        return 'no'

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_drug_treatment():
        pool = Pool()
        Configuration = pool.get('gnuhealth.contact_tracing.configuration')(1)
        if Configuration.current_campaign:
            campaign = Configuration.current_campaign
            res = [{
                'drug': x.id,
                }for x in campaign.eno_treatment_drugs]
            return res

    @staticmethod
    def default_discharge():
        return 'no'

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('patient.rec_name',) + tuple(clause[1:]),
            ]

    @staticmethod
    def default_lives_in_elderly():
        return 'no'

    @classmethod
    def __setup__(cls):
        super(ENO, cls).__setup__()
        cls._buttons.update({
            'done':{
                'invisible': Eval('state')=='done',
                },
            })

    @classmethod
    @ModelView.button
    def done(cls, vlist):
        cls.write(vlist,{
                  'state': 'done',
                      })


class ENODrugTreatment(ModelSQL,ModelView):
    'Contact Tracing Drug Treatment'
    __name__ = 'gnuhealth.contact_tracing.eno.drug_treatment'

    eno = fields.Many2One('gnuhealth.contact_tracing.eno','ENO',required=True)
    drug = fields.Many2One('gnuhealth.medicament','Drug',required=True)
    start_date = fields.Date('Start date')
    end_date = fields.Date('End date')


class ContactTracingENOParty(ModelSQL,ModelView):
    'Contact Tracing ENO Party'
    __name__ = 'gnuhealth.contact_tracing.eno-party'

    eno = fields.Many2One('gnuhealth.contact_tracing.eno','ENO',required=True)
    ref = fields.Char('PUID',required=True,
        states={'readonly':Bool(Eval('sisa_checked'))})
    lastname = fields.Char('Lastname',required=True,
        states={'readonly':Bool(Eval('sisa_checked'))})
    name = fields.Char('Name',required=True,
        states={'readonly':Bool(Eval('sisa_checked'))})
    gender = fields.Selection([
        (None,''),
        ('m','Male'),
        ('f','Female'),
        ],'Gender',required=True,
        states={'readonly': Bool(Eval('sisa_checked'))})
    phone = fields.Char('Phone',required=True)
    address = fields.Char('Address',required=True)
    last_contact_date = fields.Date('Date',required=True)
    contact_type = fields.Many2One('gnuhealth.contact_tracing.context',
        'Contact type',required=True)
    sisa_checked = fields.Function(fields.Boolean('Already in database'),'get_sisa_checked')

    def get_sisa_checked(self,name):
        pool = Pool()
        Party = pool.get('party.party')
        if self.ref:
            party = Party.search([('ref','=',self.ref)])
            if party:
                return True

    @fields.depends('ref')
    def on_change_ref(self):
        pool = Pool()
        Party = pool.get('party.party')
        if self.ref:
            party = Party.search([('ref','=',self.ref)])
            if party:
                self.name = party[0].name
                self.lastname = party[0].lastname
                self.gender = party[0].gender
                self.sisa_checked = True
                self.phone = party[0].contact_mechanisms and\
                    ' / '.join([x.value for x in party[0].contact_mechanisms if x.type in ['phone','mobile']])
                self.address = party[0].du and party[0].du.address_repr
            else:
                self.sisa_checked = False

    @classmethod
    def __setup__(cls):
        super(ContactTracingENOParty, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('contact_unique', Unique(t, t.eno, t.ref),
             'There is a contact repetead. Each contact must be unique!')
        ]
        cls._buttons.update({
            'check_sisa':{
                'invisible': Eval('sisa_checked'),
                }
            })

    #TODO check with sisa wizard
    @classmethod
    @ModelView.button
    def check_sisa(cls, vlist):
        print("\n\n\n\n\n********")
        print('Check in SISA')
