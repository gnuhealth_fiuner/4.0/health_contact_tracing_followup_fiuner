from trytond.wizard import Wizard, StateView, Button, StateTransition, StateAction
from trytond.model import ModelView, fields
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.pyson import Eval, Not, Bool, Equal
from trytond.i18n import gettext

from ..exceptions import EndDateBeforeStartDate

import csv
import sys
from io import StringIO
from datetime import datetime, date, time, timedelta


class CreateContactTracingReportsStart(ModelView):
    'Patient Following Report Start'
    __name__ = 'gnuhealth.contact_tracing.reports.start'

    campaign = fields.Many2One(
        'gnuhealth.contact_tracing.campaign','Campaign',required=True)
    report_ = fields.Selection([
        (None,''),
        ('create_call_report','Calls'),
        ('create_patient_following_report','Patient Following'),
        ('create_georeff_report','Geo Referentiation'),
        ('create_contact_tracing_stats_report','Contact Tracing Stats Report'),
        ],'Report',required=True,sort=False)
    first_contact_start = fields.DateTime('First Contact Start Date', required=True)
    first_contact_end = fields.DateTime('First Contact End Date', required=True)
    pathology = fields.Many2One('gnuhealth.pathology','Pathology',
                    states={
                        'required': Eval('report_') == 'create_contact_tracing_stats_report',
                        'invisible': Eval('report_') != 'create_contact_tracing_stats_report',
                        })
    suspected_pathology = fields.Many2One('gnuhealth.pathology',' Suspected Pathology',
                    states={
                        'required': Eval('report_') == 'create_contact_tracing_stats_report',
                        'invisible': Eval('report_') != 'create_contact_tracing_stats_report',
                        })

    @staticmethod
    def default_campaign():
        pool = Pool()
        Configuration = pool.get('gnuhealth.contact_tracing.configuration')(0)
        if Configuration.current_campaign:
            return Configuration.current_campaign.id
        return None

    @fields.depends('campaign','report_')
    def on_change_report_(self):
        if self.report_== 'create_contact_tracing_stats_report' and self.campaign: 
            self.pathology =\
                self.campaign.condition and self.campaign.condition.id
            self.suspected_pathology =\
                self.campaign.suspect_condition and self.campaign.suspect_condition.id


class CreateContactTracingReportsWizard(Wizard):
    'Call Report Wizard'
    __name__ = 'gnuhealth.contact_tracing.reports.wizard'

    @classmethod
    def __setup__(cls):
        super(CreateContactTracingReportsWizard,cls).__setup__()

    start = StateView('gnuhealth.contact_tracing.reports.start',
                      'health_contact_tracing_followup_fiuner.view_contact_tracing_report_start',[
                        Button('Cancel','end','tryton-cancel'),
                        Button('Print Report','prevalidate','tryton-ok',default=True),
                       ])

    prevalidate = StateTransition()

    create_call_report =\
        StateAction('health_contact_tracing_followup_fiuner.act_gnuhealth_contact_tracing_call_report')

    create_patient_following_report =\
        StateAction('health_contact_tracing_followup_fiuner.act_gnuhealth_contact_tracing_patient_following_report')

    create_georeff_report =\
        StateAction('health_contact_tracing_followup_fiuner.act_gnuhealth_contact_tracing_georeff_report')

    create_contact_tracing_stats_report =\
        StateAction('health_contact_tracing_followup_fiuner.act_gnuhealth_contact_tracing_stats_report')

    def default_start(self,fields):
        return{
            'first_contact_end':datetime.now()
            }

    def transition_prevalidate(self):
        if self.start.first_contact_end < self.start.first_contact_start:
            raise EndDateBeforeStartDate(
                gettext('health_contact_tracing_followup_fiuner.msg_end_date_before_start_date'))
        return self.start.report_

    def fill_data(self):
        campaign = self.start.campaign.id
        start = self.start.first_contact_start
        end = self.start.first_contact_end
        return {
            'campaign':campaign,
            'start': start, 
            'end':end 
            }

    def do_create_call_report(self, action):
        data = self.fill_data()
        return action, data

    def do_create_patient_following_report(self,action):
        data = self.fill_data()
        return action, data

    def do_create_georeff_report(self,action):
        data = self.fill_data()
        return action, data

    def do_create_contact_tracing_stats_report(self,action):
        data = self.fill_data()
        pathology = self.start.pathology and self.start.pathology.id or None
        suspected_pathology = self.start.suspected_pathology and\
            self.start.suspected_pathology.id or None
        data.update({
                'pathology': pathology,
                'suspected_pathology': suspected_pathology
                })
        return action, data
