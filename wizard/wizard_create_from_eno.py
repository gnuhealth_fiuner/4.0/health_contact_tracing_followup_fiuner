from trytond.wizard import Wizard, StateView, Button, StateTransition, StateAction
from trytond.model import ModelView, fields
from trytond.transaction import Transaction
from trytond.pool import Pool 
from trytond.pyson import Eval, Not, Bool, Equal
from trytond.i18n import gettext

from ..exceptions import NoPatientToContact

import csv
import sys
from io import StringIO
from datetime import datetime, date, time, timedelta


class CreateFromENOStart(ModelView):
    'Create From ENO - Start'
    __name__ = 'gnuhealth.contact_tracing.from_eno.start'

    patient_list = fields.One2Many(
        'gnuhealth.contact_tracing.from_eno.patient_list',None,'Patient List')


class CreateFromENOPatientList(ModelView):
    'Create From ENO - Patient List'
    __name__ = 'gnuhealth.contact_tracing.from_eno.patient_list'

    name = fields.Char('Name',readonly=True)
    lastname = fields.Char('Lastname',readonly=True)
    gender = fields.Selection([
        (None,''),
        ('f','Female'),
        ('m','Masculine'),
        ],'Gender',sort=False,readonly=True)
    ref = fields.Char('PUID',readonly=True)
    status = fields.Selection([
        (None,''),
        ('already_contacted','Already contacted'),
        ('contact_done','Contact done'),
        ('not_contacted_yet','Not contacted yet')
        ],'Status',sort=False,readonly=True)
    contact_patient = fields.Selection([
        (None,''),
        ('yes','Yes'),
        ('no','No'),
        ],'Contact patient',sort=False,required=True,
        states={
            'readonly': Eval('status') == 'already_contacted',
            })
    create_patient = fields.Selection([
        (None,''),
        ('yes','Yes'),
        ('no','No')
        ],'Create patient',sort=False,required=True,
        states={
            'readonly': Eval('create_patient') == 'no',
                })
    phone = fields.Char('Phone')
    address = fields.Char('Address')
    last_contact_date = fields.Date('Date')
    contact_type = fields.Many2One('gnuhealth.contact_tracing.context','Contact type')
    original_contact = fields.Char('Original Contact')

    @fields.depends('create_patient')
    def on_change_with_contact_patient(self):
        if self.create_patient =="no":
            return "no"

    @staticmethod
    def default_create_patient():
        return 'no'

class CreateFromENOAssignUsers(ModelView):
    'Create From ENO Assign Users'
    __name__ = 'gnuhealth.contact_tracing.from_eno.assign_users'

    assign_to_users = fields.Many2Many('res.user',None,None,
                                       'Assign to users',required=True)


class CreateFromENOWizard(Wizard):
    'Create From ENO Wizard'
    __name__ = 'gnuhealth.contact_tracing.from_eno.wizard'

    start = StateView('gnuhealth.contact_tracing.from_eno.start',
                      'health_contact_tracing_followup_fiuner.view_create_from_eno_form',[
                        Button('Cancel','end','tryton-cancel'),
                        Button('Ok','prevalidate','tryton-ok',default=True),
                       ])

    prevalidate = StateTransition()

    assign2users = StateView('gnuhealth.contact_tracing.from_eno.assign_users',
                        'health_contact_tracing_followup_fiuner.view_create_from_eno_assign2users',[
                        Button('Cancel','end','tryton-cancel'),
                        Button('Ok','create_from_eno','tryton-ok',default=True),
                        ])

    create_from_eno = StateAction('health_contact_tracing.gnuhealth_action_contact_tracing')

    def default_start(self,fields):
        pool = Pool()
        Patient = pool.get('gnuhealth.patient')
        ENO = pool.get('gnuhealth.contact_tracing.eno')
        ContactTracing = pool.get('gnuhealth.contact_tracing')
        patient_list = []

        #list the eno main patient
        eno = ENO.search([('id','in',(Transaction().context.get('active_ids')))])
        patients = list(set(x.patient for x in eno))
        #list the close contact with patients
        party_contacts = [x for y in [z.party_contacts for z in eno] for x in y]

        #list all patients id's already contacted and tracing done
        contactTracing = ContactTracing.search([('id','>',0),])
        patientAlreadyContacted = [x.patient.puid for x in contactTracing if not x.discharge]
        patientTracingDone = [x.patient.puid for x in contactTracing if x.discharge]

        #check and append all the patient on the current database
        patient_close_contact = Patient.search([('puid','in',[x.ref for x in party_contacts])])

        #append all patient currently on the database
        for patient in patients:
            pat = {
                'ref': patient.name.ref,
                'name': patient.name.name,
                'lastname': patient.name.lastname,
                'gender': patient.gender,
                'status': 'already_contacted' if patient.puid in patientAlreadyContacted\
                    else 'contact_done' if patient.puid in patientTracingDone\
                        else 'not_contacted_yet',
                'contact_patient': 'no' if patient.puid in patientAlreadyContacted else 'yes',
                'create_patient': 'no',
                }
            patient_list.append(pat)
        #party_contacts already in database
        for party in party_contacts:
            pat = {
                'name': party.name,
                'lastname': party.lastname,
                'ref': party.ref,
                'status': 'already_contacted' if party.ref in patientAlreadyContacted\
                    else 'contact_done' if party.ref in patientTracingDone\
                        else 'not_contacted_yet',
                'contact_patient': 'no' if party.ref in patientAlreadyContacted else 'yes',
                'create_patient': 'no' if party.sisa_checked else 'yes',
                'gender': party.gender,
                'phone': party.phone,
                'address': party.address,
                'contact_type': party.contact_type.id,
                'last_contact_date': party.last_contact_date,
                }
            patient_list.append(pat)
        return{
            'patient_list': patient_list
            }

    def transition_prevalidate(self):
        if not [x for x in self.start.patient_list if x.contact_patient =='yes']:
            raise NoPatientToContact(
                gettext('health_contact_tracing_followup_fiuner.no_patient_to_contact'))
        return 'assign2users'

    def do_create_from_eno(self, action):
        pool = Pool()
        ENO = pool.get('gnuhealth.contact_tracing.eno')
        eno = ENO.search([('id','in',Transaction().context.get('active_ids'))])
        campaign = eno[0].campaign.id
        Config = pool.get('gnuhealth.contact_tracing.configuration')(0)
        ContactTracing = pool.get('gnuhealth.contact_tracing')
        Party = pool.get('party.party')
        Patient = pool.get('gnuhealth.patient')

        Fedcountry = Pool().get('gnuhealth.federation.country.config')(1)
        fed_country = None
        if (Fedcountry and Fedcountry.country):
            fed_country = Fedcountry.code

        patients = Patient.search(
            [('puid','in',[x.ref for x in self.start.patient_list if x.contact_patient=='yes'])])

        new_patients = []
        parties = []
        for party in [x for x in self.start.patient_list if x.create_patient =='yes']:
            new_party = {
                'name': party.name,
                'lastname': party.lastname,
                'gender': party.gender,
                'ref': party.ref,
                'is_patient': True,
                'is_person': True,
                'fed_country': fed_country,
                'contact_mechanisms': [('create',[{
                                    'value': party.phone,
                                    'type': 'phone'}])],
                'addresses': [('create',[{
                                    'street': party.address,
                                    }])],
                }
            parties.append(new_party)
        party = Party.create(parties)
        new_patients = Patient.create([{'name': x.id} for x in party])
        patients= patients+new_patients 

        newContactTracing = []
        #TODO change to the patients on eno, not only the first one.
        #patient_eno = [x.patient for x in eno]
        patient_eno = Patient.search(['puid','=',self.start.patient_list[0].ref])[0]
        users_ = self.assign2users.assign_to_users
        for patient in patients:
            #TODO update phone number, operational_sector
            contactTracing = {
                'campaign': campaign,
                'patient': patient.id,
                'exposure_risk': 'high',
                'contact_date':\
                    [x.last_contact_date and datetime.combine(x.last_contact_date,time(12,00))\
                    for x in self.start.patient_list if x.ref == patient.puid][0],
                'contact': patient_eno.name.id if patient_eno != patient else None,
                'responsible_users': [('add',users_)],
                'phone': patient.name.contact_mechanisms and\
                    [x.value for x in patient.name.contact_mechanisms if x.type == 'phone'][0] or\
                    [x.phone for x in self.start.patient_list
                     if x.ref == patient.puid][0],
                'du': patient.name.du and patient.name.du.id\
                    or patient_eno.name.du and patient_eno.name.du.id,
                'pathology': Config.current_campaign and\
                    Config.current_campaign.suspect_condition\
                    and Config.current_campaign.suspect_condition.id\
                    if patient.id == patient_eno.id else None,
                'context_selection':[x.contact_type and x.contact_type.id\
                    for x in self.start.patient_list
                    if x.ref == patient.puid][0]
                }
            newContactTracing.append(contactTracing)
        contact_tracing = ContactTracing.create(newContactTracing)

        data = {'res_id': [ct.id for ct in contact_tracing]}
        if len(contact_tracing) == 1:
            action['views'].reverse()
        return action, data
