from trytond.wizard import Wizard, StateView, Button, StateTransition, StateAction
from trytond.model import ModelView, fields
from trytond.transaction import Transaction
from trytond.pool import Pool 

import csv
import sys
from io import StringIO
from datetime import datetime, date, time, timedelta


class CreateImportCsvStart(ModelView):
    'Create Import Csv Start'
    __name__ = 'gnuhealth.contact_tracing.import_csv.start'

    file_ = fields.Binary('File', filename='name',required=True)
    name = fields.Char('Name',readonly=True)


class CreateImportCsvPreview(ModelView):
    'Create Import Csv Preview'
    __name__ = 'gnuhealth.contact_tracing.import_csv.preview'

    contact_data = fields.One2Many(
        'gnuhealth.contact_tracing.import_csv.contact_data',None,'Contact Data')


class CreateImportCsvContactData(ModelView):
    'CSV file'
    __name__ = 'gnuhealth.contact_tracing.import_csv.contact_data'

    import_ = fields.Boolean('Import')

    patient_name = fields.Char('Patient Name',readonly=True)
    patient_lastname = fields.Char('Patient Lastname',readonly=True)
    patient_puid = fields.Char('Patient PUID',readonly=True)
    patient = fields.Many2One('gnuhealth.patient','Patient')

    caller_name = fields.Char('Caller Name',readonly=True)
    caller_lastname = fields.Char('Caller Lastname',readonly=True)
    caller_tracing = fields.Many2One('gnuhealth.healthprofessional','Caller')
    date = fields.Date('Date',readonly=True)
    temp = fields.Float('Temperature',readonly=True)
    satO2 = fields.Integer('Sat O2',readonly=True)
    heart_rate = fields.Integer('Heart Rate',readonly=True)
    pressure_diastolic = fields.Integer('Diastolic Pressure',readonly=True)
    pressure_systolic = fields.Integer('Systolic Pressure',readonly=True)
    respiratory_rate = fields.Integer('Respiratory Rate',readonly=True)
    symptoms = fields.Many2One('gnuhealth.contact_tracing.symptoms_check','Symptoms',readonly=True)
    evolution = fields.Selection([
        (None, ''),    
        ('n', 'Status Quo'),
        ('i', 'Improving'),
        ('w', 'Worsening'),
        ],'Evolution', sort=False,readonly=True)
    status = fields.Selection((
        (None, ''),  
        ('unreached', 'Unreached'),
        ('followingup', 'Following up'),
        ('na', 'Not available')
        ),'Status', sort=False,readonly=True)
    alert = fields.Boolean('Alert',readonly=True)
    verbal = fields.Boolean('Verbal',readonly=True)
    pain = fields.Boolean('Pain',readonly=True)
    awareness = fields.Boolean('Awareness',readonly=True)
    notes = fields.Text('Notes', readonly=True)

    @staticmethod
    def default_import_():
        return True


class CreateImportCsvWizard(Wizard):
    'Import Csv Start Wizard'
    __name__ = 'gnuhealth.contact_tracing.import_csv.wizard'
    
    start = StateView('gnuhealth.contact_tracing.import_csv.start',
                      'health_contact_tracing_followup_fiuner.view_create_import_csv_start_form',[
                        Button('Cancel','end','tryton-cancel'),
                        Button('Preview','prevalidate','tryton-ok',default=True),
                       ])
    
    prevalidate = StateTransition()
    
    preview = StateView('gnuhealth.contact_tracing.import_csv.preview',
                        'health_contact_tracing_followup_fiuner.view_create_import_csv_preview_form',[
                        Button('Cancel','end','tryton-cancel'),
                        Button('Import','import_data','tryton-ok',True),
                        ])
    
    import_data = StateAction('health_contact_tracing.gnuhealth_action_contact_tracing')
    
    def transition_prevalidate(self):
        return 'preview'
    
    def default_preview(self, fields):
        pool = Pool()
        HP = pool.get('gnuhealth.healthprofessional')
        Patient = pool.get('gnuhealth.patient')
        
        hps = HP.search([('id','>',0)])
        patients = Patient.search([('id','>',0)])
        file_ = self.start.file_
        if not isinstance(file_, str):
             file_ = file_.decode('utf-8')
        file_ = StringIO(file_)
        
        preview = []
        with file_ as csvfile:
            content = csv.reader(csvfile, delimiter=';')
            next(content)
            for row in content:
                p = {}
                p['satO2']=int(row[5])
                p['heart_rate'] = int(row[6])
                preview.append(p)
                
        return {
            'contact_data':preview,
            }

    def do_import_view(self, fields):        
        data = {'res_id': [ct.id for ct in contact_tracing]}
        if len(contact_tracing) == 1:
            action['views'].reverse()
        return action, data
