from trytond.exceptions import UserError


class PatientCurrentlyContacted(UserError):
    pass


class EndDateBeforeStartDate(UserError):
    pass


class NoPatientToContact(UserError):
    pass
