# -*- coding: utf-8 -*-
##############################################################################
#
#
##############################################################################

from trytond.pool import Pool
from . import configuration
from . import health_contact_tracing_followup_fiuner
from . import health_eno

from .wizard import wizard_contact_tracing_reports
from .wizard import wizard_create_from_eno
from .wizard import wizard_import_csv

from .report import call_report
from .report import contact_tracing_stats_report
from .report import discharge_report
from .report import eno_notification_report
from .report import georeff_report
from .report import isolation_report
from .report import patient_following_report


def register():
    Pool.register(
        configuration.Configuration,
        configuration.Campaign,
        configuration.ConfigurationContext,
        configuration.Context,
        configuration.ContactTracingENOMedicament,
        health_contact_tracing_followup_fiuner.ContactTracing,
        health_contact_tracing_followup_fiuner.ContactTracingCall,
        health_contact_tracing_followup_fiuner.ContactTracingSymptomsCheck,
        health_contact_tracing_followup_fiuner.ContactTracingParty,
        health_contact_tracing_followup_fiuner.ContactTracingUser,
        health_contact_tracing_followup_fiuner.ContactTracingMembers,
        wizard_import_csv.CreateImportCsvStart,
        wizard_import_csv.CreateImportCsvPreview,
        wizard_import_csv.CreateImportCsvContactData,
        health_eno.ENO,
        health_eno.ENODrugTreatment,
        health_eno.ContactTracingENOParty,
        wizard_create_from_eno.CreateFromENOStart,
        wizard_create_from_eno.CreateFromENOPatientList,
        wizard_create_from_eno.CreateFromENOAssignUsers,
        wizard_contact_tracing_reports.CreateContactTracingReportsStart,
        module='health_contact_tracing_followup_fiuner', type_='model')
    Pool.register(
        wizard_import_csv.CreateImportCsvWizard,
        wizard_create_from_eno.CreateFromENOWizard,
        wizard_contact_tracing_reports.CreateContactTracingReportsWizard,
        module='health_contact_tracing_followup_fiuner', type_='wizard')
    Pool.register(
        call_report.CallReport,
        patient_following_report.PatientFollowingReport,
        georeff_report.GeoreffReport,
        eno_notification_report.ENONotificationReport,
        discharge_report.DischargeReport,
        contact_tracing_stats_report.ContactTracingStatsReport,
        isolation_report.IsolationReport,
    	module='health_contact_tracing_followup_fiuner', type_='report')
