# -*- coding: utf-8 -*-

from trytond.pool import Pool
from trytond.report import Report


class PatientFollowingReport(Report):
    'Patient Following Report'
    __name__ = 'gnuhealth.contact_tracing.patient.following.report'

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        ContactTracing = pool.get('gnuhealth.contact_tracing')
        context = super(PatientFollowingReport, cls).get_context(records, data)
        if 'start' in data:
            start = data['start']
            end = data['end']
            contact_tracings = ContactTracing.search([
                                                ('first_contact','>=',start),
                                                ('first_contact','<=',end)
                                                ])
            context['objects'] = contact_tracings
        return context
